# Camera-control-scripts
PelcoD control client for cameras, contains a Python and a C++ implementation


## Status checklist
___Python:___
- [x] Connect to Camera manager as client
- [x] Move camera
  - [x] relative
  - [x] absolute
- [x] Implement PelcoD information receiving
- [ ] Implement PelcoD command generator
- Nice to have:
- [ ] Add a simple GUI to control

___C++:___
- [ ] Connect to Camera manager as client
- [ ] Move camera
  - [ ] relative
  - [ ] absolute
- [ ] Implement PelcoD information receiving
- [ ] Implement PelcoD command generator

## Contents
- [Python Project](#python-project)
  * [Setup of the client](#setup-of-the-client)
  * [Running the client](#running-the-client)
  * [Using the client](#using-the-client)
- [C++ Project](#c---project)
- [Used PelcoD message format](#used-pelcod-message-format)
- [Camera manager connection query](#camera-manager-connection-query)
- [Camera queries](#camera-queries)
  * [Pan](#pan)
  * [Tilt](#tilt)
  * [Zoom and Field of View](#zoom-and-field-of-view)
  * [Focus](#focus)
- [Camera commands](#camera-commands)
  * [Stop](#stop)
  * [Pan](#pan-1)
    + [Relative](#relative)
    + [Absolute](#absolute)
  * [Tilt](#tilt-1)
  * [Zoom and Field of View](#zoom-and-field-of-view-1)
  * [Focus](#focus-1)
  * [To be investigated](#to-be-investigated)
- [Acknowledgements](#acknowledgements)

<!--
<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>
-->
    
## Python Project
### Setup of the client
Global parameters for connection to the Camera Manager need to be adjusted in the `constants.py` file.

### Running the client
1. Run the Camera Manager and Video simulator
2. Run main script in python via command line:
```python client.py```
3. The client then connects to the manager using predefined commands
4. The client automatically performs a setup calibration movement while printing information about camera status to the console
5. The camera feed can be observed via network streaming (like VLC player) or via the VSD application

Currently the python client connects to the remote Axis PTZ camera and does a right turn.

___More custom commands are to be added___

### Using the client
___To be added___

## C++ Project
___To be added___

## Used PelcoD message format
Frame format contains seven bytes:		

| Byte 1 | _Byte 2_ | _Byte 3_ | _Byte 4_ | _Byte 5_ | _Byte 6_ | Byte 7 |
|---|---|---|---|---|---|---|
| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |

Where bytes 2 - 6 are Payload Bytes, like so:

```    
'synch_byte':   '\xFF',     # Synch Byte, always FF         -	1 byte
'address':      '\x00',     # Address			    -	1 byte
'command1':     '\x00',     # Command1			    -	1 byte
'command2':     '\x00',     # Command2			    -	1 byte
'data1':        '\x00',     # Data1	(PAN SPEED):	    -	1 byte
'data2':        '\x00',     # Data2	(TILT SPEED):	    - 	1 byte
'checksum':     '\x00'      # Checksum:			    -   1 byte
```
Checksum is the simple sum of the payload bytes.

In the following table there is an overview table of the structure of the commands used, with the relevant bytes highlighted. 
___LSB___ is the most significant byte and ___MSB___ is respectively the least significant byte when transferring values via the data bytes:

| Type | Command | synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum <br/> (sum of bytes 2-6)|
|:---|:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|___Query___| __Camera manager query__ | FF | 01 | 00 | __45__ | 05 | 00 | 4b |
|___Query___| __Query camera pan__ | FF | __camera address__ | 00 | __51__ | 00 | 00 | checksum |
|___Response___| __Response to camera pan query__ | FF | __camera address__ | 00 |  __59__ | __MSB__ | __LSB__ | checksum |
|___Query___| __Query camera tilt__ | FF | __camera address__ | 00 | __53__ | 00 | 00 | checksum |
|___Response___| __Response to camera tilt query__ | FF | __camera address__ | 00 |  __5B__ | __MSB__ | __LSB__ | checksum |
|___Query___| __Query camera zoom__ | FF | __camera address__ | 00 | __55__ | 00 | 00 | checksum |
|___Response___| __Response to camera zoom query__ | FF | __camera address__ | 00 |  __5D__ | __MSB__ | __LSB__ | checksum |
|___Query___| __Query camera focus__ | FF | __camera address__ | 00 | __61__ | 00 | 00 | checksum |
|___Response___| __Response to camera focus query__ | FF | __camera address__ | 00 |  __63__ | __MSB__ | __LSB__ | checksum |
|___Command___| __Stop camera__ | FF | __camera address__ | 00 | __00__ | 00 | 00 | checksum |

## Camera manager connection query
Using the following command, the connection can be tested via pinging the Camera Manager to obtain information about its version:

`\xff\x01\x00\x45\x05\x00\x4b`

The sample expected response looks like this: `TBD`

The following two commands are described in the Camera Manager documentation but are not functional:
```
# Client querying if it is communicating with a camera manager (it is):
# NOTE - Getting no response:
send_tcp(b'\xff\x01\x00\x45\x02\x00\x46')  

# Client getting the address of current controller
# NOTE - Getting no response:
send_tcp(b'\xff\x01\x00\x45\x00\x00\x47')  
```
## Camera queries
The Camera Manager can be asked about a status of a specific camera that is connected to it. 
Standard queries concern pan, tilt, zoom and focus.

### Pan

The query for _pan_ contains the following attributes:

| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| FF | camera address <br> (usually 01) | 00 | __51__ | 00 | 00 | checksum <br> (sum of bytes 2-6)|

Example command: `\xff\x01\x00\x51\x00\x00\x52`

The response is also a PelcoD message using the following structure:

| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| FF | camera address <br> (usually 01) | 00 | __59__ | __MSB__ | __LSB__ | checksum <br> (sum of bytes 2-6)|


For example, in a response `\xff\x01\x00\x59\x10\x80\xEA`, 
we get the pan position as 1080 in hex from the two data bytes. 
If we convert it to a decimal we get 4224. This indicates that the pan position is 42.24 degrees.

### Tilt
The query for _tilt_ contains the following attributes:

| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| FF | camera address <br> (usually 01) | 00 | __53__ | 00 | 00 | checksum <br> (sum of bytes 2-6)|

Example command: `\xff\x01\x00\x53\x00\x00\x54`

The response is also a PelcoD message using the following structure:

| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| FF | camera address <br> (usually 01) | 00 | __5B__ | __MSB__ | __LSB__ | checksum <br> (sum of bytes 2-6)|


For example, in a response `\xff\x01\x00\x5B\x10\x80\xEC`, 
we get the tilt position as 1080 in hex from the two data bytes. 
If we convert it to a decimal we get 4224. This would indicate that the tilt position is 42.24 degrees, 
however this is not the case in the traditional format. When considering the full scope of vertical tilt
(facing down by 90 degrees to facing up by 90 degrees), usually a scale between -90° to 90° is used. 
However, the cameras often use the 0-360° model, also based at 0° and going down. 
This difference between expected values and reported raw values can be seen on the following diagram:
![camera tilt values diagram](graphics/camera-diagram.png)

Therefore the easiest way to convert between the two is to use the following algorithm (in pseudocode):
```
if tilt_raw < 180:
    tilt = tilt_raw * -1  
else: 
    tilt = 360 - tilt_raw
```

### Zoom and Field of View
__Zoom__
The query for _zoom_ contains the following attributes:

| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| FF | camera address <br> (usually 01) | 00 | __55__ | 00 | 00 | checksum <br> (sum of bytes 2-6)|

Example command: `\xff\x01\x00\x55\x00\x00\x56`

In the simulated camera, zoom levels go from 0 (minimum zoom) to 65535 (maximum zoom).

The response is also a PelcoD message using the following structure:

| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| FF | camera address <br> (usually 01) | 00 | __5D__ | __MSB__ | __LSB__ | checksum <br> (sum of bytes 2-6)|


__Field of view__ is mapped in a non-linear, inverse relationship onto the zoom level and is camera-specific. 
Usually it is implemented via a lookup table. The lookuptable used for the simulated cameras can be visualised on the 
 following graph as an example:
![lut graph](graphics/lut-graph.png)


The field of view of a particular zoom level can therefore be calculated using the following formula:
![camera tilt values diagram](graphics/fov-formula.png)

<!--  
 source:
 http://www.sciweavers.org/free-online-latex-equation-editor
 fov_{zoom}=\frac{zoom-zoom_{min}}{zoom_{max}-zoom_{min}}  \times (fov_{maxzoom}-fov_{minzoom}) + fov_{minzoom}
 -->

Where zoom(min) and zoom(max) are the LUT defined milestones of zoom levels and fov(minzoom) and fov(maxzoom) are the corresponding FOV values.

___Example for zoom = 21111:___

Relevant part of LUT:

| Zoom | FOV |
| --- | --- |
| 20971 | 30.12 |
| 23521 | 27.95 |

```
fov(21111) = (21111-20971)/(23521-20971) * (27.95-30.12) + 30.12
           = 140/2550 * -2.17 + 30.12
           = 0.0549 * -2.17 + 30.12
           = -0.12 + 30.12
           = 30
```

### Focus
Focus is currently not implemented in the simulated camera. However, it can be queried.
The query for _focus_ contains the following attributes:

| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| FF | camera address <br> (usually 01) | 00 | __61__ | 00 | 00 | checksum <br> (sum of bytes 2-6)|

Example command: `\xff\x01\x00\x61\x00\x00\x62`

## Camera commands
The cameras can be manipulated via two types of commands. 
Relative commands specify direction in which the camera will be moving until stopped, 
whereas absolute commands specify a value to which the camera should change its state and stop on its own.

___Commands to be added___

### Stop
The command to stop the camera follows the following structure:

| synch byte | _address_ | _command1_ | _command2_ | _data1_ | _data2_ | checksum |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| FF | camera address <br> (usually 01) | 00 | __00__ | 00 | 00 | checksum <br> (sum of bytes 2-6)|

Example command: `\xff\x01\x00\x00\x00\x00\x01`
### Pan

#### Relative

#### Absolute

### Tilt

### Zoom and Field of View

### Focus

### To be investigated
Via Wireshark, these were detected and unexplained:

```
    send_tcp(b'\xff\x01\x00\x0c\x31\x03\x41')  # Camera moving up and left
    send_tcp(b'\xff\x01\x01\x88\x00\x01\x8b')  # Camera moving up
```


## Acknowledgements
- [Useful overview of standard PelcoD commands](https://www.serialporttool.com/sptblog/?p=4830)