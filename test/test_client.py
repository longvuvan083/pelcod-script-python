import pytest
from client import *

lut = load_lut('../config/cam-lookup-tables/fovLUT_sim.txt')

zoom_fov = [
    (0, 57.8),
    (1478, 55),
    (3231, 51.68),
    (4261, 50.002),
    (4391, 49.79),
    (5666, 47.87),
    (7579, 45.93),
    (9492, 43.29),
    (12293, 40.001),
    (12362, 39.92),
    (17145, 34.38),
    (20971, 30.12),
    (21111, 30.001),
    (23521, 27.95),
    (26710, 24.44),
    (29048, 22.3695),
    (30809, 20.81),
    (31774, 20.0014),
    (36275, 16.23),
    (43928, 10.76),
    (45050, 10.0011),
    (49029, 7.31),
    (53826, 5.0006),
    (55406, 4.24),
    (58427, 3.47),
    (61783, 2.7),
    (64409, 2.0001),
    (65535, 1.7),
]


@pytest.mark.parametrize("zoom, fov", zoom_fov)
def test_get_fov(zoom, fov):
    assert get_fov(zoom, lut) == pytest.approx(fov, 0.00001)


@pytest.mark.parametrize("zoom, fov", zoom_fov)
def test_get_zoom(zoom, fov):
    assert get_zoom(fov, lut) == zoom


@pytest.mark.parametrize("input_data, expected", [(0, b'\x00\x00'), (4224, b'\x10\x80'), (65535, b'\xff\xff')])
def test_dec_to_hex(input_data, expected):
    assert dec_to_hex(input_data) == expected


@pytest.mark.parametrize("input_data, expected", [(b'\x00\x00', 0), (b'\x10\x80', 4224), (b'\xff\xff', 65535)])
def test_hex_to_dec(input_data, expected):
    assert hex_to_dec(input_data) == expected


@pytest.mark.parametrize("input_data", [0, 1, 2, 5, 10, 1000, 10000, 25555, 65535])
def test_dec_to_hex_loop(input_data):
    expected = input_data
    assert hex_to_dec(dec_to_hex(input_data)) == expected
