# from libpelco import PELCO_Functions
import time

import libpelcostructs
import constants
import socket
import serial


def main():
    # TODO include struct message building

    send_tcp(b'\xff\x01\x00\x45\x05\x00\x4b', verbose=True)  # Returns cam manager id

    # send_tcp(b'\xff\x01\x00\x45\x01\x91\xd8', verbose=True) #Set Pri
    # send_tcp(b'\xff\x01\x01\x91\x00\x00\x93', verbose=True) #Set Pri
    # send_tcp(b'\xff\x01\x01\x91\x01\x00\x94', verbose=True) #Set Pri
    # send_tcp(b'\xff\x01\x01\x91\x00\x01\x94', verbose=True)  # Set Pri
    send_tcp(b'\xff\x01\x01\x91\x00\xf1\x84', verbose=True)  # Set Pri

    # send_tcp(b'\xff\x01\x00\x45\x01\x81\xc8', verbose=True) #get stat
    # send_tcp(b'\xff\x01\x01\x81\x00\x00\x83', verbose=True)  # get stat

    print("preCAB")

    send_tcp(b'\xff\x01\x01\x81\x00\x00\x83', verbose=True)
    print("END preCAB")
    # calibrate()
    print("---END OF CALIBRATION---")

    print("Check control")
    send_tcp(b'\xff\x01\x01\x81\x00\x00\x83', verbose=True)
    print("IP with control")
    send_tcp(b'\xff\x01\x01\x45\x00\x00\x47', verbose=True)

    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("Connecting socket to: ", constants.TCP_IP, ":", constants.TCP_PORT)
    soc.connect((constants.TCP_IP, constants.TCP_PORT))
    soc.send(b'\xff\x01\x00\x45\x05\x00\x4b')  # cam mgr
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)
    print("prio set to 241")
    soc.send(b'\xff\x01\x01\x91\x00\xf1\x84')  # Priority set to 241
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)

    print("Checking control")
    soc.send(b'\xff\x01\x01\x81\x00\x00\x83')
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)
    print("Checking control address")
    soc.send(b'\xff\x01\x01\x45\x00\x00\x47')
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)

    print("Moving UPRIGHT")
    soc.send(b'\xff\x01\x00\x0a\x01\x05\x11')  # Camera 1 moving right with speed 3
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)
    time.sleep(3)
    print("Stopping")
    soc.send(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)

    print("Checking control")
    soc.send(b'\xff\x01\x01\x81\x00\x00\x83')
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)
    print("Checking control address")
    soc.send(b'\xff\x01\x01\x45\x00\x00\x47')
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)

    print("Moving DOWNLEFT")
    soc.send(b'\xff\x01\x00\x14\x05\x01\x1b')  # Camera 1 moving right with speed 3
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)
    time.sleep(3)
    print("Stopping")
    soc.send(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)
    time.sleep(2)

    print("Checking control")
    soc.send(b'\xff\x01\x01\x81\x00\x00\x83')
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)
    print("Checking control address")
    soc.send(b'\xff\x01\x01\x45\x00\x00\x47')
    data = soc.recv(1024)
    print("Received:\t".expandtabs(10), data)

    time.sleep(1)
    print("Getting new controller address")
    send_tcp(b'\xff\x01\x01\x45\x00\x00\x47', verbose=True)  # get addr

    lut = load_lut('config/cam-lookup-tables/fovLUT_sonyEX1020.txt')

    get_fov(64409, lut)

    query_position()
    # Set 90 degree for Pan position
    # send_tcp(b'\xff\x01\x00\x4b\x23\x28\x97', verbose=True)
    # query_position()

    # \x5f\xf2\x5f\xf20000   ff 01 01 91 00 96 29

    print("check:", calculate_checksum(b'\xff\x01\x00\x0a\x01\x05\x12'))


def connect(address=constants.TCP_IP, port=constants.TCP_PORT):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((address, port))
    except ConnectionRefusedError:
        print("Connection refused, Camera Manager might not be running")
    return s


def send_tcp(message, timeout=constants.TIMEOUT, address=constants.TCP_IP, port=constants.TCP_PORT,
             buffer=constants.BUFFER_SIZE, verbose=False):
    """
    Function sending TCP messages

    Parameters
    ----------
    message: bytes
        message to be sent via tcp
    timeout: float, optional
        timeout in seconds (optional, otherwise taken from constants config file)
    address: str, optional
        target IP address (optional, otherwise taken from constants config file)
    port: int, optional
        target port (optional, otherwise taken from constants config file)
    buffer: int, optional
        response buffer size (optional, otherwise taken from constants config file)
    verbose: bool, optional, default=False
        if True, transmitted data is printed


    """

    # Create socket and establish TCP connection
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((address, port))

        # Send message
        s.send(message)
        s.settimeout(timeout)
        if verbose:
            print("Sent:\t".expandtabs(10), message)

        try:
            data = s.recv(buffer)
            if verbose:
                print("Received:\t".expandtabs(10), data)
            # Close connection
            s.close()
            return data
        except socket.timeout:
            if verbose:
                print("Received:\t".expandtabs(10), "No data received within timeout")
            return None

    except ConnectionRefusedError:
        print("Connection refused, Camera Manager might not be running")


def calibrate():
    print("--- CALIBRATION ---")
    # query position
    query_position()

    # move camera left and stop
    print("Camera 1 moving left")

    send_tcp(b'\xff\x01\x0a\x6b\x00\x00\x76')
    send_tcp(b'\xff\x01\x00\x04\x03\x06\x0e')  # Camera 1 moving left with speed 3
    time.sleep(3)
    send_tcp(b'\xff\x01\x0a\x6b\x00\x00\x76')
    send_tcp(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop
    query_position()

    # move camera right and stop
    print("Camera 1 moving right")
    send_tcp(b'\xff\x01\x00\x02\x03\x06\x0c')  # Camera 1 moving right with speed 3
    time.sleep(3)
    send_tcp(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop
    query_position()

    # move camera up and stop
    print("Camera 1 moving up")
    send_tcp(b'\xff\x01\x00\x08\x03\x06\x12')  # Camera 1 moving up with speed 3
    time.sleep(3)
    send_tcp(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop
    query_position()

    # move camera down and stop
    print("Camera 1 moving down")
    send_tcp(b'\xff\x01\x00\x10\x03\x06\x1a')  # Camera 1 moving down with speed 3
    time.sleep(3)
    send_tcp(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop OK
    query_position()

    # zoom in
    print("Camera 1 zooming in")
    send_tcp(b'\xff\x01\x00\x20\x00\x00\x21')  # Camera 1 moving down with speed 3
    time.sleep(3)
    send_tcp(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop OK
    query_position()

    # zoom out
    print("Camera 1 zooming in")
    send_tcp(b'\xff\x01\x00\x40\x00\x00\x41')  # Camera 1 moving down with speed 3
    time.sleep(3)
    send_tcp(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop OK
    query_position()


def query_position(lut_address="config/cam-lookup-tables/fovLUT_sim.txt"):
    try:
        # TODO add custom camera commands
        # display the current camera azimuth/tilt and fov level.
        data = send_tcp(b'\xff\x01\x00\x51\x00\x00\x52', verbose=True)[4:6]  # filters data bytes received
        pan = float(data[0] << 8 | data[1]) / 100  # combines bytes via bitwise shift and converts to degrees
        print("pan: ", pan)

        # display the current camera tilt level.
        data = send_tcp(b'\xff\x01\x00\x53\x00\x00\x54', verbose=False)[4:6]  # filters data bytes received
        tilt_raw = (float(data[0] << 8 | data[1]) / 100)
        tilt = round(tilt_raw * -1 if tilt_raw < 180 else 360 - tilt_raw, 2)
        print("tilt: ", tilt)

        # display the current camera zoom and FoV levels.
        data = send_tcp(b'\xff\x01\x00\x55\x00\x00\x56', verbose=False)[4:6]  # filters data bytes received
        zoom = float(data[0] << 8 | data[1])
        print("zoom: ", zoom)
        fov = get_fov(zoom, load_lut(lut_address))
        print("fov: ", fov)

        # Focus is unused
        focus = send_tcp(b'\xff\x01\x00\x61\x00\x00\x62', verbose=False)[4:6]  # filters data bytes received, unused now

    except TypeError:
        print("Query position failed to receive data")


def calculate_checksum(cmd):
    checksum = 0
    for byte in iter(cmd[1:6]):
        # print("byte checked:", hex(byte))
        checksum = checksum + byte
        # print("checksum:", hex(checksum))

    return hex(checksum)


def load_lut(filename):
    # Initialise variables
    zoom = []
    fov = []
    lut = []

    try:
        # Open text file with LUT
        with open(filename) as f:
            # Read file line by line
            lines = f.readlines()
            for line in lines:
                # For each line that is not commented or blank, split by whitespace and add into tokens
                if line[0] is not '#' and line is not '\n':
                    tokens = str.split(line)
                    # Add zoom and fov to custom arrays
                    zoom.append(float(tokens[0]))
                    fov.append(float(tokens[1]))
        lut.append(zoom)
        lut.append(fov)
        return lut

    except FileNotFoundError:
        print("LUT file not found at {}, check config file and fix".format(filename))


def get_fov(zoom, lut):
    for i in range(len(lut[0])):
        if i is 0:
            # If zoom is below minimal zoom, set to min
            if zoom <= lut[0][0]:
                return lut[1][0]
        elif lut[0][i] >= zoom > lut[0][i - 1]:
            # Fov is the ratio of zoom level range in, times the gap between respective fov levels (negative),
            # plus the larger fov value
            return ((zoom - lut[0][i - 1]) / (lut[0][i] - lut[0][i - 1])) * (lut[1][i] - lut[1][i - 1]) + lut[1][i - 1]
    # If zoom is beyond maximal zoom, set to max
    return lut[1][len(lut[0]) - 1]


def get_zoom(fov, lut):
    for i in range(len(lut[1])):
        if i is 0:
            # If fov is above maximal fov, set to max
            if fov >= lut[1][0]:
                return lut[0][0]
        elif lut[1][i] < fov <= lut[1][i - 1]:
            # respective equation to fov calculation
            return round(
                ((fov - lut[1][i - 1]) / (lut[1][i] - lut[1][i - 1])) * (lut[0][i] - lut[0][i - 1]) + lut[0][i - 1], 0)
    # If fov is under minimal fov, set to min
    return lut[0][len(lut[0]) - 1]


def dec_to_hex(num):
    return num.to_bytes(2, byteorder='big')


def hex_to_dec(num):
    return float(num[0] << 8 | num[1])


if __name__ == "__main__":
    main()
